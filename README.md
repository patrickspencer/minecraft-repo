## Download World

A copy of the minecraft files is here:
[http://s3.amazonaws.com/minecraft.pksg2d/minecraft-snapshot-2016-08-12.zip](http://s3.amazonaws.com/minecraft.pksg2d/minecraft-snapshot-2016-08-12.zip). It's about 243 megabytes.

## Setting up a minecraft server

### Renting server

To set up a server you have to rent one from a company. [Digital
Ocean](https://digitalocean.com) is what I used. The minecraft server was using
the $10 per month plan. You want something with at least a gig of memory. I
used Ubuntu as the server software.

Once you create a server use the root password they sent you to ssh into the
server using either a terminal (for linux or Mac) or Putty (for Windows) by
running `ssh root@(server ip)` where `(server ip)` is the ip of the server you
got from Digital Ocean. Make sure the following programs are install: wget,
java, supervisord. You can install them on Ubuntu by typing `sudo apt-get
update` then `sudo apt-get install -y wget default-jre supervisor`.

### Download Minecraft world onto server

Run `wget (url to minecraft zip mentioned above)` then
type `unzip (minecraft zip file) /root/minecraft`.

### Downloading Server

Type `cd /root/minecraft` then type `wget
https://s3.amazonaws.com/Minecraft.Download/versions/1.10.2/minecraft_server.1.10.2.jar`.
Be sure to replace the two instances of "1.10.2" in that link with the most
recent minecraft version number. You can check the most recent version by going
to
[https://minecraft.net/en/download/server](https://minecraft.net/en/download/server).
Make sure this jar file is executable by typing `chmod +x
/root/minecraft_server.jar`.

### Setup supervisord

Supervisord is a program that will automatically restart minecraft if it crashes.
First [install supervisord](http://supervisord.org/installing.html). Then copy
the file `supervisor-minecraft.conf` in this repository to the folder
`/etc/supervisor/conf.d/` then type `sudo service supervisord start`. To check
that the minecraft server is running type `supervisorctl` and then type
`status`. Hopefully "supervisor-minecraft" will show up.

## Extras

The following is a cronjob for backing up minecraft to an s3 account

```
PATH=/usr/bin:/usr/local/bin

# Run Minecraft world backup at 12 am midnight every Monday (day 1), Wednesday (day 3), and Friday (day 5)
00 00 * * 1,3,5 /bin/sh /root/sync_minecraft_files.sh >> /root/minecraft/crontab_minecraft.log 2>&1
```
